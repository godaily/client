# Introduce

This is an experiment repository to test github.com/murlokswarm/app. 
It will get data from godaily.org/api/v1/news and display in the UI.

# Installation

```
go get github.com/murlokswarm/macpack
go get github.com/godaily/client
cd $GOPATH/src/github.com/godaily/client
macpack build
open .
```

Then click the `Godaily.app`.