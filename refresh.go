package main

import (
	"github.com/lunny/log"
	"github.com/murlokswarm/app"
)

type RefreshButton struct {
	BKColor string
}

func (r *RefreshButton) Render() string {
	return `<button type="button" class="btn btn-default" aria-label="Left Align" 
	onclick="OnClick" 
	onmouseover="OnMouseOver"
    onmouseout="OnMouseOut"
    onmouseup="OnMouseUp"
    onmousedown="OnMouseDown">
  <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> 刷新
</button>`
}

func (r *RefreshButton) OnMouseOver(args app.MouseArg) {
	r.BKColor = "red"
	app.Render(r)
}

func (r *RefreshButton) OnMouseDown(args app.MouseArg) {
	r.BKColor = "green"
	app.Render(r)
}

func (r *RefreshButton) OnMouseUp(args app.MouseArg) {
	r.BKColor = "black"
	app.Render(r)
}

func (r *RefreshButton) OnMouseOut(args app.MouseArg) {
	r.BKColor = ""
	app.Render(r)
}

func (r *RefreshButton) OnClick(args app.MouseArg) {
	log.Info("RefreshButton OnClick", ListUID)
	app.Render(app.ComponentByID(ListUID))
}

func init() {
	// Registers the Hello component.
	// Allows the app to create a Hello component when it finds its declaration
	// into a HTML markup.
	app.RegisterComponent(&RefreshButton{})
}
