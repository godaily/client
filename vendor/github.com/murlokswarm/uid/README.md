# uid
[![Build Status](https://travis-ci.org/murlokswarm/uid.svg?branch=master)](https://travis-ci.org/murlokswarm/uid)
[![Go Report Card](https://goreportcard.com/badge/github.com/murlokswarm/uid)](https://goreportcard.com/report/github.com/murlokswarm/uid)
[![Coverage Status](https://coveralls.io/repos/github/murlokswarm/uid/badge.svg?branch=master)](https://coveralls.io/github/murlokswarm/uid?branch=master)
[![GoDoc](https://godoc.org/github.com/murlokswarm/uid?status.svg)](https://godoc.org/github.com/murlokswarm/uid)

Package to generate unique identifiers.