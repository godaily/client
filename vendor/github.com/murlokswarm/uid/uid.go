// Package uid is a package to generate unique identifiers.
// It defines the Generator type, which handles unique identifier generation.
//
// The package also have predefined generators reflecting the most common
// purposes related to the iu package, accessible by helper functions such as
// Window() or Elem().
package uid

var (
	context = NewGenerator("context")
	elem    = NewGenerator("elem")
)

// ID represents an identifier.
type ID string

func (id ID) String() string {
	return string(id)
}

// Context generates an unique identifier that should be used in a context.
func Context() ID {
	return context.Generate()
}

// Elem generates an unique identifier that should be used in elements.
func Elem() ID {
	return elem.Generate()
}
