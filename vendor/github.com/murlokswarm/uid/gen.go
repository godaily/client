package uid

import (
	"fmt"
	"sync"
)

// Generator represents an object that generates an unique identifier relative
// to its scope.
// A Generator can be used simultaneously from multiple goroutines.
type Generator struct {
	Name  string
	index uint64
	mtx   sync.Mutex
}

// Generate generates a unique identifier.
func (g *Generator) Generate() ID {
	g.mtx.Lock()
	defer g.mtx.Unlock()

	g.index++
	id := fmt.Sprintf("%v-%v", g.Name, g.index)
	return ID(id)
}

// NewGenerator creates a new Generator.
// The name appears at the beginning of each generated id.
func NewGenerator(name string) *Generator {
	return &Generator{
		Name: name,
	}
}
