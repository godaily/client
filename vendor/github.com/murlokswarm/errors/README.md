# errors
[![Build Status](https://travis-ci.org/murlokswarm/errors.svg?branch=master)](https://travis-ci.org/murlokswarm/errors)
[![Go Report Card](https://goreportcard.com/badge/github.com/murlokswarm/errors)](https://goreportcard.com/report/github.com/murlokswarm/errors)
[![Coverage Status](https://coveralls.io/repos/github/murlokswarm/errors/badge.svg?branch=master)](https://coveralls.io/github/murlokswarm/errors?branch=master)
[![GoDoc](https://godoc.org/github.com/murlokswarm/errors?status.svg)](https://godoc.org/github.com/murlokswarm/errors)

Package to create and format errors.
