package main

import (
	"github.com/murlokswarm/app"
	_ "github.com/murlokswarm/mac"
)

var (
	win app.Contexter
)

func main() {
	app.OnLaunch = func() {
		appMenu := &AppMainMenu{}    // Creates the AppMainMenu component.
		app.MenuBar().Mount(appMenu) // Mounts the AppMainMenu component into the application menu bar.

		appMenuDock := &AppMainMenu{} // Creates another AppMainMenu.
		app.Dock().Mount(appMenuDock)

		win = newMainWindow() // Create the main window.
	}

	app.OnReopen = func(hasVisibleWindow bool) {
		if win != nil {
			return
		}

		win = newMainWindow()
	}

	app.Run()
}

func newMainWindow() app.Contexter {
	// Creates a window context.
	win := app.NewWindow(app.Window{
		Title:          "Godaily",
		Width:          640,
		Height:         720,
		TitlebarHidden: false,
		OnClose: func() bool {
			win = nil
			return true
		},
	})

	list := &NewsList{} // Creates a NewsList component.
	win.Mount(list)     // Mounts the NewsList component into the window context.
	ListUID = app.ComponentID(list)
	return win
}
