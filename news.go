package main

import (
	"net/http"
	"time"

	"encoding/json"

	"github.com/lunny/log"
	"github.com/murlokswarm/app"
	"github.com/murlokswarm/uid"
)

var (
	ListUID uid.ID
)

type NewsInfo struct {
	Id          int64
	Title       string
	Image       string
	Url         string
	Author      string
	AuthorLink  string
	Author2     string
	Author2Link string
	Updated     time.Time
}

// Hello implements app.Componer interface.
type NewsList struct {
	Contents []NewsInfo
	Error    string
}

func (h *NewsList) LoadData() error {
	log.Info("loading data from godaily.org ...")
	// FIXME: load data from network
	resp, err := http.Get("http://godaily.org/api/v1/news")
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	h.Contents = make([]NewsInfo, 0)

	err = json.NewDecoder(resp.Body).Decode(&h.Contents)
	if err != nil {
		return err
	}

	return nil
}

// Render returns the HTML markup that describes the appearance of the
// component.
// It supports standard HTML and extends it slightly to handle other component
// declaration or Golang callbacks.
// Can be templated following rules from https://golang.org/pkg/text/template.
func (h *NewsList) Render() string {
	err := h.LoadData()
	if err != nil {
		h.Error = err.Error()
	}

	return `
<div class="container-fluid" style="margin-top:20px;background-image: url(../bg2.png)">
	<div class="row">
		<div class="col-md-12">
			<RefreshButton></RefreshButton>
		</div>
	</div>
	<div class="row" style="margin-top:20px">
{{if .Contents}}
{{range .Contents}}  
<div class="media col-md-12">
  <div class="media-left">
    <img class="media-object" src="{{.Image}}"/>
  </div>
  <div class="media-body">
    <h4 class="media-heading"><a href="{{.Url}}">{{html .Title}}</a></h4>
    <div>{{.Author}} {{time .Updated "2006-01-02"}}</div>
  </div>
</div>
{{end}}
{{else if .Error}}
<div>Load data error: {{.Error}}</div>
{{else}}
<div>No data</div>
{{end}}
</div>
</div>
    `
}

func init() {
	// Registers the Hello component.
	// Allows the app to create a Hello component when it finds its declaration
	// into a HTML markup.
	app.RegisterComponent(&NewsList{})
}
