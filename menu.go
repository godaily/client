package main

import "github.com/murlokswarm/app"

// AppMainMenu implements app.Componer interface.
type AppMainMenu struct {
	CustomTitle string
	Disabled    bool
}

// Render returns the HTML markup that describes the appearance of the
// component.
// In this case, the component will be mounted into a menu context.
// This restrict the markup to a compositon of menu and menuitem.
func (m *AppMainMenu) Render() string {
	return `
<menu>
    <menu label="app">
        <menuitem label="{{if .CustomTitle}}{{.CustomTitle}}{{else}}关于{{end}}" 
                  onclick="OnCustomMenuClick"
                  separator="true"
                  disabled="{{.Disabled}}" />
        <menuitem label="退出" shortcut="meta+q" selector="terminate:" />        
    </menu>
</menu>
    `
}

type AboutDiv struct {
}

func (a *AboutDiv) Render() string {
	return `<div class="media">
  <div class="media-left">
    <a href="http://godaily.org">
      <img class="media-object" src="logo.png" width="64" height="64" alt="..."/>
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">Godaily</h4>
    <div>copyright @ 2017 godaily authors</div>
  </div>
  </div>
`
}

// OnCustomMenuClick is the handler called when an onclick event occurs in a menuitem.
func (m *AppMainMenu) OnCustomMenuClick() {
	about := app.NewWindow(app.Window{
		Title:          "关于",
		Width:          300,
		Height:         240,
		TitlebarHidden: false,
		MinimizeHidden: true,
		FixedSize:      true,
		X:              0,
		Y:              0,
		OnClose: func() bool {
			return true
		},
	})

	about.Mount(&AboutDiv{})
}

// WindowMenu implements app.Componer interface.
// It's another component which will be nested inside the AppMenu component.
type WindowMenu struct {
}

func (m *WindowMenu) Render() string {
	return `
<menu label="Window">
    <menuitem label="Close" selector="performClose:" shortcut="meta+w" />
</menu>
    `
}

func init() {
	// Allows the app to create a AppMainMenu and WindowMenu components when it finds its declaration
	// into a HTML markup.
	app.RegisterComponent(&AppMainMenu{})
	app.RegisterComponent(&WindowMenu{})
	app.RegisterComponent(&AboutDiv{})
}
